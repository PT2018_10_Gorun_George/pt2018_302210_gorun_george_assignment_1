package control;
import data.Polinom;
import data.Monom;
import java.util.ArrayList;

public class ParsePolinom {
public ParsePolinom() {}
    public ArrayList<Monom> parsePolinom(String s){

        ArrayList<Monom> monoms = new ArrayList<>();

        String[] splits = s.split("\\+");
        for (int i = 0; i < splits.length; i++){
            String[] parts = splits[i].split("X\\^");
            if (parts.length > 0){
               monoms.add(new Monom( Float.parseFloat(parts[0]),Integer.parseInt(parts[1])));
            }
            else monoms.add(new Monom( Float.parseFloat(parts[0]),0));
        }

        return monoms;
    }

}
